#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "Usage ./script.sh {PROJECT_NAME}"
    exit 2
fi

find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s/#PROJECT_NAME/$1/g" {} +